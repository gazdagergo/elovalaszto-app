<?php
namespace App\Controller;

use App\Controller\AppController;
use DateTime;
/**
 * Votes Controller
 *
 * @property \App\Model\Table\VotesTable $Votes
 *
 * @method \App\Model\Entity\Vote[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VotesController extends AppController
{

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vote = $this->Votes->newEntity();
        $user = $this->Authentication->getIdentity();
        if (!$this->Authorization->can($user, 'voteOnline')) {
            $this->loadModel('Users');
            if ($user->voted) {
                $this->loadModel('Places');
                $placename = $this->Places->get($this->Authentication->getIdentity()->place_voted);
                $this->set('placename', $placename);
            }
            $reasons = $this->Users->canVote($this->Authentication->getIdentity()->id);
            if (!$user->voted && is_array($reasons)) {
                $this->set('reasons', $reasons);
            }

        }
        $candidates = $this->Votes->Candidates->find();
        $this->set(compact('vote', 'candidates', 'canVote'));
    }

    public function castvote() {
        $this->loadModel('Users');
        $vote = $this->Votes->newEntity();
        $user = $this->Authentication->getIdentity();
        if ($this->Authorization->can($user, 'VoteOnline')) {
            if ($this->request->is('POST')) {
                $vote = $this->Votes->patchEntity($vote, $this->request->getData());
                $result = $this->Votes->getConnection()->transactional(function () use ($vote, $user) {
                    if ($user->voted) {
                        $this->Flash->error(__('Nem lehet újabb szavazatot leadni'));
                        return false;
                    } else {
                        $user->voted = true;
                        $user->vote_time = new DateTime('now');
                        $user->place_voted = 1;
                        $vote->zip = $user->zip;
                        $vote->place_id = 1;
                        $finalvote = $this->Votes->save($vote);
                        $info = $this->Users->save($user);
                        $this->Authentication->setIdentity($info);
                        return $finalvote;
                    }
                });
                if ($result) {
                    $this->Flash->success(__('A törzsállományba bekerült a szavazatod!'));
                    $this->redirect(['action' => 'add']);
                } else {
                    $this->Flash->error(__('A szavazatot nem lehetett elmenteni, adatbázis hiba miatt'));
                    $this->redirect(['action' => 'add']);
                }
            }
        } else {
            $this->Flash->error(__('A szavazás nem lehetséges. Ennek okai a következő oldalon kerülnek összefoglalásra '));
            $this->redirect(['action' => 'add']);
        }

    }   

}
