        <section class="section">
          <div class="section-header">
            <h1>A Magyarhang Nonprofit Kft. 2019-es előválasztásra vonatkozó adatkezelési szabályzata</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
                <!-- </p> -->
                <p>A jelen – előválasztásra vonatkozó – adatkezelési szabályzat a Magyarhang Nonprofit Kft. általános adatkezelési szabályzatának 2019. június 12. napján kelt kiegészítése, azzal együtt érvényes.</p>

                <p>A Budapesti Előválasztás szervezője a Civil Választási Bizottság (CVB). Ezt a testületet civil közösségek és ellenzéki pártok képviselői hozták létre. A CVB jelenlegi tagszervezetei:</p>
                <ul>
                  <li>Civilek a demokráciáért</li>
                  <li>Mindenki Magyarországa Mozgalom</li>
                  <li>Nyomtass te is!</li>
                  <li>aHang Platform</li>
                  <li>Oktogon Közösség</li>
                  <li>Kálmán Olga kampánystábja</li>
                  <li>Karácsony Gergely kampánystábja</li>
                  <li>Kerpel-Fronius Gábor kampánystábja</li>
                  <li>Szolidaritás Mozgalom</li>
                  <li>DK</li>
                  <li>Jobbik</li>
                  <li>LMP</li>
                  <li>Momentum</li>
                  <li>MSZP</li>
                  <li>Párbeszéd</li>
                  <li>Civil Választási Bizottság</li>
                </ul>

                <p>Az előválasztást a Civil Választási Bizottság a Magyarhang Nonprofit Kft. (székhely: 1071 Budapest, Damjanich utca 52. IV/1, telefonszám: +36702033476, képviselő: Varga Máté ügyvezető) segítségével bonyolítja le. A Magyarhang Nonprofit Kft. a cél érdekében adatkezelőként és adatfeldolgozóként kezeli a jelölteket ajánlók, a szavazásra regisztrálók, illetve a szavazók egyes személyes adatait.</p>

                <p>Az előválasztás offline lebonyolítását a CVB tagszervezetei végzik, míg az előválasztás online lebonyolítására a Magyarhang Nonprofit Kft. létrehozta az elovalaszto.hu weboldalt. Az online és offline begyűjtött adatok összesítését és feldolgozását a Magyarhang Nonprofit Kft. végzi.</p>

                <div class="section-title mt-0" style="margin-bottom: 15px;">I. Az adatkezelés jogalapja, célja és határideje</div>

                <p><strong>A Magyarhang Nonprofit Kft. előválasztással kapcsolatos adatkezelésének és adatfeldolgozásának célja, hogy az előválasztás második fordulója során civil kontroll mellett kerüljön kiválasztásra a változást akaró többség főpolgármester-jelöltje.</strong></p>

                <p><strong>Az adatkezelés és az adatfeldolgozás jogalapja:</strong> A Magyarhang Nonprofit Kft. a jelen szabályzatban részletezett személyes adatokat az érintettek kifejezett hozzájárulása alapján kezeli és dolgozza fel. A jog a személyes adatok meghatározott körét „érzékenynek” ismeri el, melyek nagyobb védelmet élveznek. Ilyen érzékeny adat a politikai vélemény, melyről az előválasztást  lebonyolító Magyarhang Nonprofit Kft. információt szerezhet. <strong>Az előválasztáson résztvevők a jelen adatkezelési szabályzat és a Magyarhang Nonprofit Kft. általános adatvédelmi szabályzatának elfogadásával kifejezett beleegyezésüket adják ahhoz, hogy személyes adataikat, köztük a politikai véleményüket a továbbiakban a Magyarhang Nonprofit Kft.  e szabályzatok alapján és keretei között kezelje és feldolgozza</strong> (GDPR 9. cikk (2) bekezdés a) pont). </p>

                <p><strong>Az előválasztással kapcsolatos adatkezelés és az adatfeldolgozás az előválasztás lebonyolításáig, azaz 2019. június 26-áig, illetve az azt követő jogorvoslati időszak végéig, azaz legkésőbb 2019. július 1. napjáig tart, azt követően a begyűjtött és tárolt személyes adatok megsemmisítésre kerülnek.</strong></p>

                <div class="section-title mt-0" style="margin-bottom: 15px;">II. Az adatkezelés folyamata</div>

                <p><strong>1. A szavazáshoz történő online regisztráció az elovalaszto.hu weboldalon:</strong></p>

                <p>A felhasználóknak a saját ügyfélkapujukon keresztül a személyadat- és lakcímnyilvántartásból letöltött, majd azonosításra visszavezetett dokumentumhitelesítés szolgáltatással (AVDH) hitelesített, a kormányzati portál által rendszeresített „tájékoztató személyiadat- és lakcímnyilvántartásban tárolt aktuális adatokról” belügyminisztériumi pdf dokumentumot kell az elovalaszto.hu oldalra feltölteniük.</p>

                <p>A feltöltött pdf AVDH aláírásának érvényességét a rendszer emberi közbeavatkozás nélkül ellenőrzi, majd amennyiben az érvényesnek bizonyul, a fájlból a következő adatokat olvassa ki és tárolja el ugyancsak emberi közbeavatkozás nélkül:</p>

                <ul>
                  <li>a lakcímkártya számának vissza nem fejthető karaktersorozata (hash),<sup>1</sup></li>
                  <li>a születési dátum,</li>
                  <li>az irányítószám,</li>
                  <li>a lakcímkártya számának utolsó két karaktere.</li>
                </ul>

                <p>Ezek az <strong>előválasztás adatbázisába kerülő adatok önmagukban általunk már nem köthetőek konkrét személyhez</strong>, azoknak a konkrét személlyel való összekötése számunkra nem is lehetséges, ezért azok a tárolt formájukban nem is személyes adatok.</p>

                <p>Ezeknek az adatoknak az előválasztás adatbázisában való rögzítése után az <strong>eredeti feltöltött pdf-et a rendszer emberi közbeavatkozás nélkül, elkülönítve eltárolja egy kiemelt biztonsággal rendelkező alfiókba</strong> (általában a másodperc töredéke alatt lezajlik a hitelesítés, az adatok kinyerése az előválasztás adatbázisába, a pdf-ek elkülönítése és alfiókba való biztonsági tárolása). A jelen bekezdésben foglalt adatkezelés jogalapja az 1996. évi XX. törvény a személyazonosító jel helyébe lépő azonosítási módokról és az azonosító kódok használatáról 7. § (2) és 35. § szerinti hozzájárulás, célja a lakcímek lakcím-nyilvántartóban való szúrópróbaszerű csoportos ellenőrzése, annak biztosítása érdekében, hogy kizárólag budapesti lakosok vehessenek részt a szavazáson. Az alfiókhoz kizárólag az ellenőrzést végző személyek férnek hozzá, az ellenőrzés időtartamáig és céljából. Azok a felhasználók, akik a jelen bekezdésben foglalt adatkezeléshez nem járulnak hozzá, lehetőségük van arra, hogy offline (személyesen helyszínen) szavazzanak, vagy a következő pont szerint offline (személyes helyszíni) regisztrációt követően szavazzanak online.</p>

                <p>A felhasználó továbbá megadja a weboldalon:</p>

                <ul>
                  <li>szabadon választott felhasználónév,</li>
                  <li>egy e-mail cím (erre kerül kiküldésre az aktiváló link, mellyel a felhasználó jelszót tud megadni fiókjához, illetve be tud lépni fiókjába a szavazáshoz),</li>
                  <li>egy telefonszám (melyet aktivistáink felhívhatnak, hogy ellenőrizzék az adatok helyességét).</li>
                </ul>

                <p>A felhasználónak joga van megtekinteni az összes róla nyilvántartott adatot a felhasználói fiókjában, továbbá törölheti személyes adatait a rendszerből. A biztonsági alfiókban tárolt pdf törlését az elovalaszto@ahang.hu email címen kérhető. Törlés után elfelejtett jelszó esetén nem tudunk új aktiváló linket küldeni.</p>

                <p>Az regisztráció során azon felhasználók, akik kipipálják a <strong>„Szeretnék értesítéseket kapni az előválasztás fejleményeiről és az aHang kezdeményezéseiről telefonon / emailben (ez nagyon fontos, mert csak így tudunk veled kapcsolatban maradni).” jelölőnégyzetet</strong>, azokra atovábbiakban a Magyarhang Nonprofit Kft.általános adatvédelmi szabályzata, illetve az <a href="https://ahang.hu/" target="_blank"><strong>ahang.hu</strong></a> weboldalára vonatkozó érvényes <a href="https://ahang.hu/adatkezeles/" target="_blank"><strong>adatvédelmi irányelvek és felhasználási feltételek</strong></a> vonatkoznak.</p>

                <p><strong>2. Offline (helyszíni személyes) regisztráció az online szavazásra:</strong></p>

                <p>A regisztrálás során a CVB aktivistái elektronikusan rögzítik:</p>

                <ul>
                  <li>a szabadon választható felhasználónevet,</li>
                  <li>az irányítószámot,</li>
                  <li>a lakcímkártya számból képzett hash-t,</li>
                  <li>az e-mail címét (erre küldjük ki a jelszó megadásához szükséges aktiváló linket, ezért ez itt kötelező),</li>
                  <li>a születési dátumot,</li>
                  <li>az adatkezelési hozzájárulást, illetve az adatkezelési szabályzatok elfogadásának tényét. Ez utóbbi kifejezett hozzájárulás oly módon történik, hogy a helyszíneken a papír alapon is elérhetővé tett szabályzatok megismerése és elolvasása után az aktivista elektronikus eszközén az offline regisztráló saját maga pipálja ki a megfelelő jelölőnégyzetet.</li>
                </ul>

                <p><strong>3. Az offline (helyszíni személyes) szavazás:</strong></p>

                <p>Az offline szavazás során a CVB aktivistái elektronikusan rögzítik:</p>

                <ul>
                  <li>az irányítószámot,</li>
                  <li>a születési dátumot,</li>
                  <li>a lakcímkártya számból képzett hash-t,</li>
                  <li>az e-mail címet (nem kötelező megadni, csak abban az esetben, ha szeretne értesítéseket kapni az előválasztás fejleményeiről és az aHang Platform kezdeményezéseiről e-mailben),</li>
                  <li>az adatkezelési hozzájárulást, illetve az adatkezelési szabályzatok elfogadásának tényét. Ez utóbbi kifejezett hozzájárulás oly módon történik, hogy a helyszíneken a papír alapon is elérhetővé tett szabályzatok megismerése, és elolvasása után az aktivista elektronikus eszközén az offline szavazó saját maga pipálja ki a megfelelő jelölőnégyzetet.</li>
                </ul>

                <p><strong>4. A jelöltajánlás:</strong></p>

                <p>Jelöltet ajánlhat (akár több jelöltet is) minden budapesti előválasztó, aki adatait (név, születési idő, cím, telefonszám, aláírás) megadja egy (vagy több) jelölt ajánlásgyűjtő ívén, aláírásával az ajánlásgyűjtő íven ellenjegyzi, illetve elfogadja, hogy a személyes adatai, köztük politikai véleménye kezeléséhez és feldolgozásához kifejezetten hozzájárul. A Magyarhang Nonprofit Kft. a beérkezett ajánlásgyűjtő íveket elkülönítve, biztonságosan tárolja, és csak abból a célból kezeli és dolgozza fel, hogy a jelölteket kiállítsa, illetve a jelöltajánlókat szúrópróbaszerűen, telefonos megkeresés útján ellenőrizze.</p>

                <div class="section-title mt-0" style="margin-bottom: 15px;">III. Egyéb</div>

                <p>A Magyarhang Nonprofit Kft. az előválasztáshoz kapcsolódó adatkezelésben, az adatfeldolgozásban és az ellenőrzésben részt vállaló aktivistákkal önkéntes szerződést köt, amely a fokozott titoktartási kötelezettségről is rendelkezik.</p>

                <p><strong>A magyarhang Nonprofit Kft. az adatkezelés során adatfeldolgozóként  veszi igénybe az EZIT Kft.</strong> (székhely: H-1132 Budapest, Victor Hugo u. 18-22.; képviselő: Adolf  Szilveszter Attila; cégjegyzékszám: Cg. 01-09-968191) <strong>tárhely szolgáltatását.</strong> A felhőszerver Magyarország területén, magyar joghatóság alatt található.</p>

              <!-- </p> -->
              </div>
              <div class="card-footer bg-whitesmoke">
                <sup>1</sup> A lakcímkártya számát biztonsági okokból nem tároljuk a szavazás rendszerében. Egy matematikai algoritmus segítségével egy vissza nem fejthető karaktersorozatot (hash) állítunk elő belőle. Egy adott lakcímkártyaszám mindig ugyanazt a hash-t fogja adni, így biztosítható, hogy lakcímkártya számok tárolása nélkül megvalósítsuk az egy felhasználó = egy szavazó elvet.
              </div>
            </div>
          </div>
        </section>