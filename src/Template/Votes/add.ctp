        <section class="section">
          <div class="section-header">
            <h1>
                <?php echo ($this->Identity->get('voted')) ? __('Jelöltek') : __('Szavazat leadása') ?>
            </h1>
          </div>
          <div class="section-body">
            <h2 class="section-title">

                <?php
                    if ($this->Identity->get('voted')) {
                       echo  __('A szavazatodat ekkor adtad le: {0}, helyszín: {1}', [$this->Time->format($this->Identity->get('vote_time'),  \IntlDateFormatter::FULL,  null,  'Europe/Budapest'), $placename->name]);
                    } else if (isset($reasons)) {
                        foreach ($reasons as $reason) {
                            // echo $reason .  '<br>';
                            echo '<p class="section-lead elovalaszto-custom">' . $reason .  '</p>';
                        }
                    } else {
                        echo __('Kérjük az alábbi listából válaszd ki, akire szeretnél szavazni!');
                    }
                 ?>
            </h2>
            <div class="row">
                <?php if (empty($candidates)): ?>
                    <div class="col-12 col-sm-12 col-lg-12 text-center">
                        <div class="card card-light">
                            <div class="card-header">
                                <h4 class="center"><?php echo __('Még nincsenek jelöltek a rendszerben'); ?></h4>
                            </div>
                        </div>

                    </div>

                <?php else : ?>                    

                    <?php foreach ($candidates as $candidate): ?>
                    <div class="col-12 col-sm-6 col-lg-3 text-center">
                        <div class="card card-light">
                            <div class="card-header">
                                <h4 class="center"><?php echo $candidate->name; ?></h4>
                            </div>
                            <div class="card-body">
                                <?php echo $candidate->party; ?>
                            </div>
                            <div class="card-footer bg-whitesmoke" style="text-align: center;">

                                <?php 
                                    $class = 'btn btn-primary';
                                    if ($this->Identity->get('voted')) $class .= ' disabled';
                                    if (isset($reasons)) $class .= ' disabled';
                                    echo $this->Form->postLink($candidate->name . ' ' . __('jelöltre szavazok'), ['action' => 'castvote'], ['class' => $class, 'data' => ['candidate_id' => $candidate->id],'confirm' => __('Biztos, hogy {0} jelöltre adod le a szavazatod?', [$candidate->name])]) ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                <?php endif ?>


            </div>
          </div>
        </section>