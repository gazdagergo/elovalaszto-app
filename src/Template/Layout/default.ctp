<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" sizes="196x196" href="/favicon-196x196.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
    <link rel="icon" type="image/png" sizes="128x128" href="/favicon-128x128.png" />
    <meta name="application-name" content="Előválasztó"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="/mstile-310x310.png" />
    <meta property="og:locale" content="hu_HU"/>
    <meta property="og:site_name" content="Előválasztó"/>
    <meta property="og:title" content="Előválasztó - szavazz előre!"/>
    <meta property="og:url" content="https://elovalaszto.hu/"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="Előválasztó - szavazz előre!"/>
    <meta property="og:image" content="/elovalaszto-default-open-graph-1200x628.jpg"/>
    <meta property="og:image:url" content="/elovalaszto-default-open-graph-1200x628.jpg"/>
    <meta property="og:image:secure_url" content="/elovalaszto-default-open-graph-1200x628.jpg"/>
    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="628"/>
    <meta property="article:publisher" content="https://www.facebook.com/szabadahang/"/>
    <meta itemprop="name" content="Előválasztó - szavazz előre!"/>
    <meta itemprop="headline" content="Előválasztó - szavazz előre!"/>
    <meta itemprop="description" content="Előválasztó - szavazz előre!"/>
    <meta itemprop="image" content="/elovalaszto-default-open-graph-1200x628.jpg"/>
    <meta itemprop="author" content="aHang"/>
    <meta name="twitter:title" content="Előválasztó - szavazz előre!"/>
    <meta name="twitter:url" content="https://elovalaszto.hu/"/>
    <meta name="twitter:description" content="Előválasztó - szavazz előre!"/>
    <meta name="twitter:image" content="/elovalaszto-default-open-graph-1200x628.jpg"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@szabadahang"/>
    <link rel="canonical" href="https://elovalaszto.hu/"/>
    <meta name="description" content="Előválasztó - szavazz előre!"/>
    <meta name="author" content="aHang"/>
    <meta name="publisher" content="Előválasztó"/>
    <title>
        Előválasztó: 
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->css('//fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,tamil,vietnamese') ?>
    <?= $this->Html->css('//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css') ?>
    <?= $this->Html->css('//use.fontawesome.com/releases/v5.7.2/css/all.css') ?>
    <?= $this->Html->css('assets/css/style.min.css') ?>
    <?= $this->Html->css('assets/css/components.min.css') ?>
    <?= $this->Html->css('style.min.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?= $this->Html->script('//code.jquery.com/jquery-3.3.1.min.js') ?>
</head>
<body class="elovalaszto <?php echo (isset($this->request->getParam('pass')[0]) && ($this->request->getParam('pass')[0] === 'home')) ? 'display' : $this->request->getParam('pass'[0]); ?>">
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="far fa-bell"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
              <div class="dropdown-header">Fontos értesítések
              </div>
              <div class="dropdown-list-content dropdown-list-icons">
                <a href="#" class="dropdown-item dropdown-item-unread">
                  <div class="dropdown-item-icon bg-primary text-white">
                    <i class="fas fa-code"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    Aláírások gyűjtése jelölteknek
                    <div class="time text-primary">2019. június 5. – június 17.</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <div class="dropdown-item-icon bg-info text-white">
                    <i class="far fa-user"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    Előregisztráció online szavazásra
                    <div class="time">2019. június 12. reggel 8 és június 20. reggel 8 óra között</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <div class="dropdown-item-icon bg-success text-white">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    Online szavazás
                    <div class="time">2019. június 20. reggel 8 és június 26. déli 12 óra között</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <div class="dropdown-item-icon bg-danger text-white">
                    <i class="fas fa-exclamation-triangle"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    Offline szavazás
                    <div class="time">2019.június 20. reggel 8 és június 26. déli 12 óra között</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <div class="dropdown-item-icon bg-danger text-white">
                    <i class="fas fa-exclamation-triangle"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    Szavazatszámlálás és eredményhirdetés
                    <div class="time">2019. június 26., szerda, 14:00-20:00 között</div>
                  </div>
                </a>
              </div>
            </div>
          </li>
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg"><i class="fas fa-user"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
              <div class="dropdown-header">
                <?php
            if ($this->Identity->isLoggedIn()) {
              echo $this->Html->link(__('Üdvözöljük, ' . $this->Identity->get('username') . '! Itt lépjen tovább a felhasználói fiókhoz!'), ['controller' => 'users', 'action' => 'dashboard'], ['class' => 'btn btn-outline-primary btn-elovalaszto-global-topright', 'escape'=>false]);
            } else { 
              echo $this->Html->link(__('Regisztrált már? Lépjen be!'), ['controller' => 'users', 'action' => 'bejelentkezes'], ['class' => 'btn btn-outline-primary btn-elovalaszto-global-topright', 'escape'=>false]) ;
            }
            ?>
              </div>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand" style="margin-bottom: 10px;">
            <div class="elovalaszto-logomark-animation-container" style="height: 49.61px; width: 249.97px;">
                <a href="/">
                    <svg class="elovalaszto-logotype" style="height: 49.61px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="-95.041 -149 524.041 104" width="524.041px" height="104px">
                        <g>
                            <rect x="-95.041" y="-149" width="524.041" height="103.999" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"/>
                            <path d=" M 28.819 -72 L 55.811 -72 L 54.76 -78.748 L 37.447 -78.748 L 37.447 -87.542 L 50.445 -87.542 L 49.395 -94.345 L 37.447 -94.345 L 37.447 -102.808 L 53.598 -102.808 L 52.547 -109.611 L 28.819 -109.611 L 28.819 -72 Z  M 87.393 -72 L 86.398 -78.748 L 70.081 -78.748 L 70.081 -109.611 L 61.452 -109.611 L 61.452 -72 L 87.393 -72 Z  M 108.688 -110.441 L 108.688 -110.441 Q 103.157 -110.386 98.843 -107.841 L 98.843 -107.841 L 98.843 -107.841 Q 94.529 -105.297 92.122 -100.817 L 92.122 -100.817 L 92.122 -100.817 Q 89.716 -96.337 89.716 -90.64 L 89.716 -90.64 L 89.716 -90.64 Q 89.716 -85.219 92.012 -80.794 L 92.012 -80.794 L 92.012 -80.794 Q 94.307 -76.369 98.594 -73.797 L 98.594 -73.797 L 98.594 -73.797 Q 102.88 -71.225 108.688 -71.225 L 108.688 -71.225 L 108.688 -71.225 Q 114.219 -71.281 118.534 -73.797 L 118.534 -73.797 L 118.534 -73.797 Q 122.848 -76.314 125.254 -80.794 L 125.254 -80.794 L 125.254 -80.794 Q 127.66 -85.274 127.66 -91.027 L 127.66 -91.027 L 127.66 -91.027 Q 127.66 -96.447 125.392 -100.872 L 125.392 -100.872 L 125.392 -100.872 Q 123.124 -105.297 118.838 -107.869 L 118.838 -107.869 L 118.838 -107.869 Q 114.551 -110.441 108.688 -110.441 L 108.688 -110.441 Z  M 98.677 -90.695 L 98.677 -90.695 Q 98.677 -94.401 100.06 -97.249 L 100.06 -97.249 L 100.06 -97.249 Q 101.442 -100.098 103.738 -101.646 L 103.738 -101.646 L 103.738 -101.646 Q 106.033 -103.195 108.799 -103.195 L 108.799 -103.195 L 108.799 -103.195 Q 111.786 -103.195 114.026 -101.619 L 114.026 -101.619 L 114.026 -101.619 Q 116.266 -100.042 117.483 -97.277 L 117.483 -97.277 L 117.483 -97.277 Q 118.7 -94.511 118.7 -91.027 L 118.7 -91.027 L 118.7 -91.027 Q 118.7 -86.934 117.372 -84.113 L 117.372 -84.113 L 117.372 -84.113 Q 116.045 -81.292 113.749 -79.882 L 113.749 -79.882 L 113.749 -79.882 Q 111.454 -78.471 108.633 -78.471 L 108.633 -78.471 L 108.633 -78.471 Q 104.042 -78.471 101.359 -81.873 L 101.359 -81.873 L 101.359 -81.873 Q 98.677 -85.274 98.677 -90.695 L 98.677 -90.695 Z  M 98.843 -113.981 L 104.982 -112.709 L 110.79 -122.775 L 102.106 -122.775 L 98.843 -113.981 Z  M 111.232 -113.981 L 117.372 -112.709 L 123.18 -122.775 L 114.496 -122.775 L 111.232 -113.981 Z  M 148.512 -82.951 L 138.556 -110.164 L 129.983 -109.169 L 144.309 -72 L 152.439 -72 L 166.765 -109.611 L 158.192 -109.611 L 148.512 -82.951 Z  M 190.383 -109.611 L 182.252 -109.611 L 167.871 -72 L 176.5 -72 L 179.431 -80.13 L 193.093 -80.13 L 196.135 -71.447 L 204.653 -72.442 L 190.383 -109.611 Z  M 186.124 -98.66 L 190.549 -86.989 L 181.865 -86.989 L 186.124 -98.66 Z  M 182.805 -113.981 L 188.945 -112.709 L 194.752 -122.775 L 186.069 -122.775 L 182.805 -113.981 Z  M 235.683 -72 L 234.687 -78.748 L 218.37 -78.748 L 218.37 -109.611 L 209.742 -109.611 L 209.742 -72 L 235.683 -72 Z  M 259.301 -109.611 L 251.17 -109.611 L 236.789 -72 L 245.418 -72 L 248.349 -80.13 L 262.011 -80.13 L 265.053 -71.447 L 273.571 -72.442 L 259.301 -109.611 Z  M 255.042 -98.66 L 259.467 -86.989 L 250.783 -86.989 L 255.042 -98.66 Z  M 276.336 -74.378 L 276.336 -74.378 Q 278.328 -73.161 281.895 -72.221 L 281.895 -72.221 L 281.895 -72.221 Q 285.463 -71.281 288.062 -71.281 L 288.062 -71.281 L 288.062 -71.281 Q 292.543 -71.281 295.834 -72.663 L 295.834 -72.663 L 295.834 -72.663 Q 299.125 -74.046 300.867 -76.48 L 300.867 -76.48 L 300.867 -76.48 Q 302.609 -78.914 302.609 -82.011 L 302.609 -82.011 L 302.609 -82.011 Q 302.609 -84.887 301.005 -87.127 L 301.005 -87.127 L 301.005 -87.127 Q 299.401 -89.367 297.299 -90.833 L 297.299 -90.833 L 297.299 -90.833 Q 295.198 -92.299 291.215 -94.622 L 291.215 -94.622 L 291.215 -94.622 Q 290.275 -95.23 289.39 -95.618 L 289.39 -95.618 L 289.39 -95.618 Q 287.012 -96.89 285.933 -97.802 L 285.933 -97.802 L 285.933 -97.802 Q 284.854 -98.715 284.854 -99.932 L 284.854 -99.932 L 284.854 -99.932 Q 284.854 -101.425 286.154 -102.283 L 286.154 -102.283 L 286.154 -102.283 Q 287.454 -103.14 289.501 -103.14 L 289.501 -103.14 L 289.501 -103.14 Q 291.492 -103.14 293.206 -102.697 L 293.206 -102.697 L 293.206 -102.697 Q 294.921 -102.255 297.576 -101.149 L 297.576 -101.149 L 300.231 -107.454 L 300.231 -107.454 Q 298.018 -108.782 295.087 -109.584 L 295.087 -109.584 L 295.087 -109.584 Q 292.155 -110.386 289.39 -110.386 L 289.39 -110.386 L 289.39 -110.386 Q 285.297 -110.386 282.255 -109.031 L 282.255 -109.031 L 282.255 -109.031 Q 279.213 -107.675 277.581 -105.325 L 277.581 -105.325 L 277.581 -105.325 Q 275.949 -102.974 275.894 -99.987 L 275.894 -99.987 L 275.894 -99.987 Q 275.894 -97.166 277.526 -94.899 L 277.526 -94.899 L 277.526 -94.899 Q 279.157 -92.631 281.287 -91.137 L 281.287 -91.137 L 281.287 -91.137 Q 283.416 -89.644 287.177 -87.376 L 287.177 -87.376 L 287.177 -87.376 Q 288.284 -86.712 288.837 -86.436 L 288.837 -86.436 L 288.837 -86.436 Q 291.381 -84.943 292.515 -84.058 L 292.515 -84.058 L 292.515 -84.058 Q 293.649 -83.173 293.649 -82.122 L 293.649 -82.122 L 293.649 -82.122 Q 293.649 -80.518 292.155 -79.55 L 292.155 -79.55 L 292.155 -79.55 Q 290.662 -78.582 287.897 -78.582 L 287.897 -78.582 L 287.897 -78.582 Q 285.85 -78.582 283.223 -79.384 L 283.223 -79.384 L 283.223 -79.384 Q 280.595 -80.186 278.825 -81.126 L 278.825 -81.126 L 276.336 -74.378 Z  M 335.188 -72 L 334.303 -78.748 L 314.225 -78.748 L 335.022 -103.195 L 334.081 -109.611 L 306.094 -109.611 L 306.979 -102.808 L 324.844 -102.808 L 304.49 -78.36 L 305.485 -72 L 335.188 -72 Z  M 368.872 -102.808 L 367.821 -109.611 L 336.515 -109.611 L 336.515 -102.808 L 347.743 -102.808 L 347.743 -72 L 356.427 -72 L 356.427 -102.808 L 368.872 -102.808 Z  M 390.167 -110.441 L 390.167 -110.441 Q 384.636 -110.386 380.322 -107.841 L 380.322 -107.841 L 380.322 -107.841 Q 376.007 -105.297 373.601 -100.817 L 373.601 -100.817 L 373.601 -100.817 Q 371.195 -96.337 371.195 -90.64 L 371.195 -90.64 L 371.195 -90.64 Q 371.195 -85.219 373.491 -80.794 L 373.491 -80.794 L 373.491 -80.794 Q 375.786 -76.369 380.073 -73.797 L 380.073 -73.797 L 380.073 -73.797 Q 384.359 -71.225 390.167 -71.225 L 390.167 -71.225 L 390.167 -71.225 Q 395.698 -71.281 400.012 -73.797 L 400.012 -73.797 L 400.012 -73.797 Q 404.327 -76.314 406.733 -80.794 L 406.733 -80.794 L 406.733 -80.794 Q 409.139 -85.274 409.139 -91.027 L 409.139 -91.027 L 409.139 -91.027 Q 409.139 -96.447 406.871 -100.872 L 406.871 -100.872 L 406.871 -100.872 Q 404.603 -105.297 400.317 -107.869 L 400.317 -107.869 L 400.317 -107.869 Q 396.03 -110.441 390.167 -110.441 L 390.167 -110.441 Z  M 380.156 -90.695 L 380.156 -90.695 Q 380.156 -94.401 381.538 -97.249 L 381.538 -97.249 L 381.538 -97.249 Q 382.921 -100.098 385.217 -101.646 L 385.217 -101.646 L 385.217 -101.646 Q 387.512 -103.195 390.278 -103.195 L 390.278 -103.195 L 390.278 -103.195 Q 393.264 -103.195 395.504 -101.619 L 395.504 -101.619 L 395.504 -101.619 Q 397.745 -100.042 398.961 -97.277 L 398.961 -97.277 L 398.961 -97.277 Q 400.178 -94.511 400.178 -91.027 L 400.178 -91.027 L 400.178 -91.027 Q 400.178 -86.934 398.851 -84.113 L 398.851 -84.113 L 398.851 -84.113 Q 397.523 -81.292 395.228 -79.882 L 395.228 -79.882 L 395.228 -79.882 Q 392.933 -78.471 390.112 -78.471 L 390.112 -78.471 L 390.112 -78.471 Q 385.521 -78.471 382.838 -81.873 L 382.838 -81.873 L 382.838 -81.873 Q 380.156 -85.274 380.156 -90.695 L 380.156 -90.695 Z  M 386.627 -113.981 L 392.767 -112.709 L 398.574 -122.775 L 389.89 -122.775 L 386.627 -113.981 Z " fill="rgb(67,111,77)"/>
                            <rect x="-95.041" y="-149" width="104" height="104" transform="matrix(1,0,0,1,0,0)" fill="rgb(205,42,62)"/>
                        </g>
                    </svg>
                    <svg class="elovalaszto-logomark elovalaszto-logomark-animation" style="height: 49.61px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="44.512 -345 104 104" width="104px" height="104px">
                        <g>
                            <g opacity="0">
                                <rect x="44.512" y="-345" width="104" height="104" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"/>
                            </g>
                            <path d=" M 106.099 -273.905 L 103.898 -284.435 L 115.764 -292.843 L 103.898 -301.172 L 106.099 -311.623 L 128.65 -295.593 L 128.65 -290.093 L 106.099 -273.905 L 106.099 -273.905 L 106.099 -273.905 L 106.099 -273.905 Z  M 83.154 -318.774 L 83.154 -318.774 Q 93.055 -318.774 100.755 -313.116 L 100.755 -313.116 L 94.941 -304.864 L 94.941 -304.864 Q 89.048 -308.793 84.018 -308.793 L 84.018 -308.793 L 84.018 -308.793 Q 80.875 -308.793 79.147 -307.498 L 79.147 -307.498 L 79.147 -307.498 Q 77.418 -306.201 77.418 -304.237 L 77.418 -304.237 L 77.418 -304.237 Q 77.418 -301.8 79.815 -300.347 L 79.815 -300.347 L 79.815 -300.347 Q 82.211 -298.894 86.219 -298.894 L 86.219 -298.894 L 92.033 -298.894 L 90.776 -289.229 L 85.04 -289.229 L 85.04 -289.229 Q 80.875 -289.229 78.675 -287.578 L 78.675 -287.578 L 78.675 -287.578 Q 76.475 -285.927 76.475 -282.942 L 76.475 -282.942 L 76.475 -282.942 Q 76.475 -280.349 78.675 -278.739 L 78.675 -278.739 L 78.675 -278.739 Q 80.875 -277.127 84.097 -277.127 L 84.097 -277.127 L 84.097 -277.127 Q 87.162 -277.127 89.873 -277.991 L 89.873 -277.991 L 89.873 -277.991 Q 92.583 -278.856 95.884 -280.899 L 95.884 -280.899 L 102.327 -272.726 L 102.327 -272.726 Q 98.162 -270.213 93.291 -268.72 L 93.291 -268.72 L 93.291 -268.72 Q 88.419 -267.226 84.176 -267.226 L 84.176 -267.226 L 84.176 -267.226 Q 78.282 -267.226 73.804 -269.231 L 73.804 -269.231 L 73.804 -269.231 Q 69.325 -271.235 66.849 -274.769 L 66.849 -274.769 L 66.849 -274.769 Q 64.374 -278.306 64.374 -282.785 L 64.374 -282.785 L 64.374 -282.785 Q 64.374 -286.714 66.535 -290.093 L 66.535 -290.093 L 66.535 -290.093 Q 68.696 -293.472 71.368 -294.649 L 71.368 -294.649 L 71.368 -294.649 Q 69.089 -295.828 67.164 -298.854 L 67.164 -298.854 L 67.164 -298.854 Q 65.239 -301.88 65.239 -304.944 L 65.239 -304.944 L 65.239 -304.944 Q 65.239 -308.873 67.399 -312.015 L 67.399 -312.015 L 67.399 -312.015 Q 69.56 -315.159 73.607 -316.966 L 73.607 -316.966 L 73.607 -316.966 Q 77.654 -318.774 83.154 -318.774 L 83.154 -318.774 L 83.154 -318.774 L 83.154 -318.774 L 83.154 -318.774 Z " fill-rule="evenodd" fill="rgb(255,255,255)"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
        <div class="sidebar-brand sidebar-brand-sm" style="margin-bottom: 10px;">
            <div class="elovalaszto-logomark-animation-container" style="height: 45px; width: 45px; background-color: #cd2a3e; top: 10px; left: 10px;">
                <a href="/">
                    <svg class="elovalaszto-logomark elovalaszto-logomark-animation" style="height: 45px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="44.512 -345 104 104" width="104px" height="104px">
                        <g>
                            <g opacity="0">
                                <rect x="44.512" y="-345" width="104" height="104" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"/>
                            </g>
                            <path d=" M 106.099 -273.905 L 103.898 -284.435 L 115.764 -292.843 L 103.898 -301.172 L 106.099 -311.623 L 128.65 -295.593 L 128.65 -290.093 L 106.099 -273.905 L 106.099 -273.905 L 106.099 -273.905 L 106.099 -273.905 Z  M 83.154 -318.774 L 83.154 -318.774 Q 93.055 -318.774 100.755 -313.116 L 100.755 -313.116 L 94.941 -304.864 L 94.941 -304.864 Q 89.048 -308.793 84.018 -308.793 L 84.018 -308.793 L 84.018 -308.793 Q 80.875 -308.793 79.147 -307.498 L 79.147 -307.498 L 79.147 -307.498 Q 77.418 -306.201 77.418 -304.237 L 77.418 -304.237 L 77.418 -304.237 Q 77.418 -301.8 79.815 -300.347 L 79.815 -300.347 L 79.815 -300.347 Q 82.211 -298.894 86.219 -298.894 L 86.219 -298.894 L 92.033 -298.894 L 90.776 -289.229 L 85.04 -289.229 L 85.04 -289.229 Q 80.875 -289.229 78.675 -287.578 L 78.675 -287.578 L 78.675 -287.578 Q 76.475 -285.927 76.475 -282.942 L 76.475 -282.942 L 76.475 -282.942 Q 76.475 -280.349 78.675 -278.739 L 78.675 -278.739 L 78.675 -278.739 Q 80.875 -277.127 84.097 -277.127 L 84.097 -277.127 L 84.097 -277.127 Q 87.162 -277.127 89.873 -277.991 L 89.873 -277.991 L 89.873 -277.991 Q 92.583 -278.856 95.884 -280.899 L 95.884 -280.899 L 102.327 -272.726 L 102.327 -272.726 Q 98.162 -270.213 93.291 -268.72 L 93.291 -268.72 L 93.291 -268.72 Q 88.419 -267.226 84.176 -267.226 L 84.176 -267.226 L 84.176 -267.226 Q 78.282 -267.226 73.804 -269.231 L 73.804 -269.231 L 73.804 -269.231 Q 69.325 -271.235 66.849 -274.769 L 66.849 -274.769 L 66.849 -274.769 Q 64.374 -278.306 64.374 -282.785 L 64.374 -282.785 L 64.374 -282.785 Q 64.374 -286.714 66.535 -290.093 L 66.535 -290.093 L 66.535 -290.093 Q 68.696 -293.472 71.368 -294.649 L 71.368 -294.649 L 71.368 -294.649 Q 69.089 -295.828 67.164 -298.854 L 67.164 -298.854 L 67.164 -298.854 Q 65.239 -301.88 65.239 -304.944 L 65.239 -304.944 L 65.239 -304.944 Q 65.239 -308.873 67.399 -312.015 L 67.399 -312.015 L 67.399 -312.015 Q 69.56 -315.159 73.607 -316.966 L 73.607 -316.966 L 73.607 -316.966 Q 77.654 -318.774 83.154 -318.774 L 83.154 -318.774 L 83.154 -318.774 L 83.154 -318.774 L 83.154 -318.774 Z " fill-rule="evenodd" fill="rgb(255,255,255)"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
        <ul class="sidebar-menu">
              <li class="menu-header">Felhasználó</li>
              <?php if ($this->Identity->isLoggedIn()): ?>
                <li>
                  <?php echo $this->Html->link('<i class="fas fa-tachometer-alt"></i>' . __('<span>Fiók</span>'), ['controller' => 'users', 'action' => 'dashboard'], ['class' => 'nav-link', 'escape' => false]) ?>
                </li>
                <li>
                  <?php echo $this->Html->link('<i class="fas fa-sign-out-alt fa-flip-horizontal"></i>' . __('<span>Kijelentkezés</span>'), ['controller' => 'users', 'action' => 'kijelentkezes'], ['class' => 'nav-link', 'escape' => false]) ?>
                </li>
              <?php else : ?>   
                <li>
                  <?php echo $this->Html->link('<i class="fas fa-edit"></i>' . __('<span>Regisztráció</span>'), ['controller' => 'users', 'action' => 'pdf'], ['class' => 'nav-link', 'escape' => false]) ?>
                </li>
                <li>
                  <?php echo $this->Html->link('<i class="fas fa-sign-in-alt"></i>' . __('<span>Bejelentkezés</span>'), ['controller' => 'users', 'action' => 'bejelentkezes'], ['class' => 'nav-link', 'escape' => false]) ?>
                </li>             
              <?php endif ?>

              <li class="menu-header">Előválasztás</li>
              <li class="nav-item">
                <?php echo $this->Html->link('<i class="fas fa-lightbulb"></i>' . __('<span>Tudnivalók</span>'), ['controller' => 'pages', 'action' => 'display', 'tudnivalok'], ['class' => 'nav-link', 'escape' => false]) ?>
              </li>
              <li class="nav-item">
                <?php echo $this->Html->link('<i class="fas fa-coins"></i>' . __('<span>Támogass minket!</span>'), ['controller' => 'pages', 'action' => 'display', 'tamogass'], ['class' => 'nav-link', 'escape' => false]) ?>
              </li>
              <li class="nav-item">
                <?php echo $this->Html->link('<i class="fas fa-eye"></i>' . __('<span>Adatkezelés</span>'), ['controller' => 'pages', 'action' => 'display', 'adatkezeles'], ['class' => 'nav-link', 'escape' => false]) ?>
              </li>
              <li class="nav-item">
                <?php echo $this->Html->link('<i class="fas fa-folder-open"></i>' . __('<span>Presskit</span>'), ['controller' => 'pages', 'action' => 'display', 'presskit'], ['class' => 'nav-link', 'escape' => false]) ?>
              </li>
            </ul>

            <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
              <a href="https://gitlab.com/elovalaszto/" target="_blank" class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fab fa-gitlab"></i> GitLab
              </a>
            </div>
        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
            
        </div>
        <div class="footer-right">
          <span>Szavazás: 2019.06.20-26. Regisztráció ONLINE szavazásra: 2019.06.20. 8:00-ig. Info: elovalaszto@ahang.hu<br /><br /></span>
          Copyright &copy; 2019 Előválasztó - szavazz előre! Fejlesztés és üzemeltetés: <?php echo $this->Html->image('ahang.hu-horizontal.svg', ['url' => 'https://ahang.hu/', 'style' => 'height: 20px; position: relative; top: -2px;']) ?> Minden jog fenntartva.
        </div>
      </footer>
    </div>
  </div>

    <?= $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js') ?>
    <?= $this->Html->script('//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js') ?>
    <?= $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js') ?>
    <?= $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') ?>
    <?= $this->Html->script('assets/js/stisla.js') ?>
    <?= $this->Html->script('assets/js/scripts.js') ?>
    <?= $this->Html->script('assets/js/custom.js') ?>
    <?= $this->Html->script('matomo') ?>
    <?= $this->fetch('scriptbottom') ?>
</body>
</html>